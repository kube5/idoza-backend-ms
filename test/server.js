var expect    = require("chai").expect;
var greetings = require("../app/greetings");


describe("Check Greetings function", function() {
  it("greeting.greet should be: greetings!", function() {
    expect(greetings.greet()).to.equal('greetings6!');
  });
});